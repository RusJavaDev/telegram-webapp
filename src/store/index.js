import { createStore } from 'vuex';
import { routers } from "@/routes";

const cart = JSON.parse(localStorage.getItem('cart')) || []
const selectedWishes = {}
cart.forEach(el =>  selectedWishes[el.id] = el.quantity)

export const store = createStore({
    state() {
        return {
            userId: null,
            userBonusPoints: null,
            menu: [],
            categoryNames: [],
            selectedWishes: selectedWishes,
            selectedWishModal: null,
            cart: {
                items: cart || [],
            },
            totalSum: null,
            selectedUserId: null,
            selectedCategoryModal: null
        }
    },

    mutations: {
        setMenu(state, menu) {
            state.menu = menu
        },

        concatMenu(state, menu) {
            const arr = state.menu.concat(menu)
            state.menu = arr
        },

        setCategoryNames(state, categoryNames) {
            state.categoryNames = categoryNames
        },

        incrementQuantity(state, wish) {
            if (!state.selectedWishes[wish.id]) {
                state.selectedWishes[wish.id] = 1;
            } else {
                state.selectedWishes[wish.id]++;
            }
        },

        incrementQuantityInCart(state, wish) {
            const existingItem = state.cart.items.find(item => item.id === wish.id);
            if (existingItem) {
                existingItem.quantity++;
            } else {
                state.cart.items.push({ ...wish, quantity: 1 });
            }
            localStorage.setItem('cart', JSON.stringify(state.cart.items))
        },

        decrementQuantity(state, wish) {
            if (state.selectedWishes[wish.id]) {
                state.selectedWishes[wish.id]--;
            }
        },

        decrementQuantityInCart(state, wish) {
            const existingItem = state.cart.items.find(item => item.id === wish.id);

            if (existingItem) {
                if (existingItem.quantity > 1) {
                    existingItem.quantity--;
                } else {
                    state.cart.items.splice(state.cart.items.indexOf(existingItem), 1);
                }
            }

            localStorage.setItem('cart', JSON.stringify(state.cart.items))

            if (state.cart.items.length === 0) {
                routers.push({ name: 'Menu' });
            }
        },

        removeWish(state, wish) {
            delete state.selectedWishes[wish.id];
        },

        removeWishFromCart(state, wish) {
            const index = state.cart.items.findIndex(item => item.id === wish.id);
            if (index !== -1) {
                state.cart.items.splice(index, 1);
            }
            localStorage.setItem('cart', JSON.stringify(state.cart.items))
        },

        setSelectedWishModal(state, wish) {
            state.selectedWishModal = wish;
        },

        setTotalSum(state, totalSum) {
            state.totalSum = totalSum;
        },

        setUserId(state, userId) {
            state.userId = userId;
        },

        setUserBonusPoints(state, userBonusPoints) {
            state.userBonusPoints = userBonusPoints;
        },

        setSelectedUserId(state, userId) {
            state.selectedUserId = userId
        },

        setSelectedCategoryModal(state, category) {
            state.selectedCategoryModal = category;
        },
    },
    actions: {
        setMenu(context, menu) {
            context.commit('setMenu', menu)
        },

        concatMenu(context, menu) {
            context.commit('concatMenu', menu)
        },

        setCategoryNames(context, categoryNames) {
            context.commit('setCategoryNames', categoryNames)
        },

        incrementQuantity(context, wish) {
            context.commit('incrementQuantityInCart', wish)
            context.commit('incrementQuantity', wish)
        },

        decrementQuantity(context, wish) {
            context.commit('decrementQuantityInCart', wish);
            context.commit('decrementQuantity', wish);
        },

        removeWish(context, wish) {
            context.commit('removeWishFromCart', wish);
            context.commit('removeWish', wish);
        },

        setSelectedWishModal(context, selectedWishModal) {
            context.commit('setSelectedWishModal', selectedWishModal);
        },

        setUserId(context, userId) {
            context.commit('setUserId', userId);
        },

        setUserBonusPoints(context, userBonusPoints) {
            context.commit('setUserBonusPoints', userBonusPoints);
        },

        setSelectedUserId(context, userId) {
            context.commit('setSelectedUserId', userId)
        },

        setSelectedCategoryModal(context, category) {
            context.commit('setSelectedCategoryModal', category)
        },
    },
    getters: {
        getMenu(state) {
            return state.menu;
        },

        getCategoryNames(state) {
            return state.categoryNames;
        },

        getSelectedWishes(state) {
            return state.selectedWishes;
        },

        getCart(state) {
            return state.cart.items;
        },

        getTotalSum(state) {

            return state.cart.items.reduce((total, item) => total + (item.cost + (item.selectedOptions ? item.selectedOptions.reduce((sum, option) => sum + option.optionCost, 0) : 0)) * item.quantity, 0);
        },

        isWishesWithoutDelivery(state) {
            return state.cart.items.filter(wish => wish.availableForDelivery === false);
        },

        getUserId(state) {
            return state.userId;
        },

        getUserBonusPoints(state) {
            return state.userBonusPoints;
        },

        getSelectedWishModal(state) {
            return state.selectedWishModal;
        },

        getSelectedUserId(state) {
            return state.selectedUserId
        },

        getSelectedCategoryModal(state) {
            return state.selectedCategoryModal;
        },
    }
});

