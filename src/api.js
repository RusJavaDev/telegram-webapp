import axios from 'axios';
import {routers} from '@/routes';
import './index.scss';

const api = axios.create();

api.interceptors.request.use(config => {
    if (localStorage.getItem('access_token')) {
        config.headers = {
            Authorization: `Bearer ${JSON.parse(localStorage.getItem('access_token')).token}`,
        };
    }

    return config;
});

api.interceptors.response.use(config => {
    if (localStorage.getItem('access_token')) {
        config.headers = {
            Authorization: `Bearer ${JSON.parse(localStorage.getItem('access_token')).token}`,
        };
    }

    return config;
}, error => {
    if (error.response.status === 401) {
        routers.push({name: 'Login'});
    }
})

export default api;