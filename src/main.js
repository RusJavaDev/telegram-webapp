import {createApp} from 'vue'
import App from './App.vue'
import {routers} from './routes';
import {store} from './store';

import 'vuetify/styles'
import 'vuetify/dist/vuetify.min.css'
import YmapPlugin from 'vue-yandex-maps'
import {createVuetify} from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import {aliases, mdi} from 'vuetify/iconsets/mdi-svg'
import {
    mdiAccountGroup,
    mdiArrowLeft,
    mdiCalculator,
    mdiFood,
    mdiCertificateOutline,
    mdiSilverwareForkKnife,
    mdiTune,
    mdiCogOutline,
    mdiOfficeBuildingCogOutline,
    mdiMagnify,
    mdiAlertCircleOutline
} from '@mdi/js'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import '@mdi/font/css/materialdesignicons.css'


const vuetify = createVuetify({
    components,
    directives,
    icons: {
        defaultSet: 'mdi',
        aliases: {
            ...aliases,
            people: mdiAccountGroup,
            arrow: mdiArrowLeft,
            orders: mdiCalculator,
            goods: mdiSilverwareForkKnife,
            food: mdiFood,
            category: mdiCertificateOutline,
            setting: mdiTune,
            commonSetting: mdiCogOutline,
            sections: mdiOfficeBuildingCogOutline,
            search: mdiMagnify,
            alert: mdiAlertCircleOutline
        },
        sets: {
            mdi,
        },
    },
})

const settings = {
    apiKey: '66778f35-2fac-47df-9534-ae2a251e0996',
    lang: 'ru_RU',
    coordorder: 'latlong',
    debug: false,
    version: '2.1'
}


const app = createApp(App);
app.use(routers)
app.use(store)
app.use(vuetify)
app.use(YmapPlugin, settings)

app.mount('#app')
