import {createRouter, createWebHistory} from 'vue-router';
import { store } from './store';
import ProfilePage from '@/pages/profile.vue';
import MenuPage from '@/pages/menu.vue';
import CartPage from '@/pages/cart.vue'
import OrderPage from '@/pages/order.vue'
import UserOrdersPage from '@/pages/userOrders.vue';
import LoginPage from '@/pages/login.vue'
import RegisterPage from '@/pages/register.vue'
import AdminDashBoardPage from '@/pages/adminDashboard.vue'
import UserProfile from '@/components/UserProfile.vue'
import UsersTable from '@/components/UsersTable.vue'
import OrdersTable from '@/components/OrdersTable.vue'
import UserOrderCard from '@/components/UserOrderCard.vue'
import Contacts from '@/pages/contacts.vue'

const routerHistory = createWebHistory();

export const routers = createRouter({
    history: routerHistory,
    routes: [
        {
            path: '/profile',
            name: 'Profile',
            component: ProfilePage
        },
        {
            path: '/cart',
            name: 'Cart',
            component: CartPage,
            beforeEnter: (to, from, next) => {
                const cartItems = store.getters.getCart;

                if (cartItems.length === 0) {
                    next({name: 'Menu'});
                }else {
                    next();
                }
            }
        },
        {
            path: '/menu',
            name: 'Menu',
            component: MenuPage
        },
        {
            path: '/order',
            name: 'Order',
            component: OrderPage
        },
        {
            path: '/myOrders',
            name: 'MyOrders',
            component: UserOrdersPage
        },
        {
            path: '/login',
            name: 'Login',
            component: LoginPage
        },
        {
            path: '/register',
            name: 'Registration',
            component: RegisterPage
        },
        {
            path: '/dashboard',
            name: 'AdminDashboard',
            component: AdminDashBoardPage
        },
        {
            path: '/userProfile',
            name: 'UserProfile',
            component: UserProfile
        },
        {
            path: '/users',
            name: 'UsersTable',
            component: UsersTable
        },
        {
            path: '/orders',
            name: 'OrdersTable',
            component: OrdersTable
        },
        {
            path: '/userOrderCard',
            name: 'UserOrderCard',
            component: UserOrderCard
        },
        {
            path: '/contacts',
            name: 'Contacts',
            component: Contacts
        },
    ]
})
